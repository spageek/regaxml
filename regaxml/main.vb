﻿' Reg-a-XML es una aplicación para realizar la conversión de ficheros en formato Windows
' Registry Editor Version 5.00 a XML, con la estructura necesária para importar en la 
' consola de GPO de Active Directory.
'
' Este programa ha sido escrito por Jose Ramón Lambea, puedes encontrar el códio fuente
' en el siguiente repositorio GIT: https://bitbucket.org/spageek/regaxml.git
'
' Cualquier sugerencia de mejora, o reporte de error o bug, por favor, informadlo en:
' https://bitbucket.org/spageek/regaxml/issues
'
' ¡Gracias =)!
' newbie@spageek.net
' 130929
Imports System.Xml
Module main

    Sub Main()

        ' Recogida de parámetros de entrada
        If My.Application.CommandLineArgs.Count <> 1 Then
            Console.WriteLine("Uso: " & System.Reflection.Assembly.GetEntryAssembly().GetName().Name & " REG_FILE")
            System.Environment.Exit(2)
        End If

        ' El primer parámetro será el fichero de entrada
        Dim inFile As String = My.Application.CommandLineArgs(0)

        ' Validamos el fichero de entrada, según si existe y si se puede sacar alguna clave
        If Not IO.File.Exists(My.Application.CommandLineArgs(0)) Then
            Console.WriteLine("El fichero " & inFile & " no existe.")
            System.Environment.Exit(3)
        ElseIf regaxml.countKeys(inFile) = 0 Then
            Console.WriteLine("Fichero " & inFile & " sin claves de registro válidas.")
            System.Environment.Exit(4)
        End If

        ' Definimos el fichero de salida con el mismo nombre que el de entrada pero con extensión xml
        Dim outFile As String = IO.Path.GetDirectoryName(inFile) & "\" & IO.Path.GetFileNameWithoutExtension(inFile) & ".xml"
        Dim fXML As New XmlTextWriter(outfile, System.Text.Encoding.UTF8)

        ' Inicio de fichero
        fXML.Formatting = Formatting.Indented
        fXML.Indentation = 2
        fXML.WriteStartDocument()
        fXML.WriteStartElement("Collection")
        fXML.WriteStartAttribute("clsid")
        fXML.WriteValue("{53B533F5-224C-47e3-B01B-CA3B3F3FF4BF}")
        fXML.WriteStartAttribute("name")
        fXML.WriteValue(IO.Path.GetFileName(inFile))

        ' Generación de contenido del xml según el fichero reg
        For Each key As String In regparser.getKeys(inFile)
            If getKeyContent(inFile, key).Count > 0 Then
                For Each element As item In getKeyContent(inFile, key)
                    fXML.WriteStartElement("Registry")
                    fXML.WriteStartAttribute("clsid")
                    fXML.WriteValue(element.clsid)
                    fXML.WriteStartAttribute("name")
                    fXML.WriteValue(element.name)
                    fXML.WriteStartAttribute("status")
                    fXML.WriteValue(element.name)
                    fXML.WriteStartAttribute("image")
                    fXML.WriteValue(element.image)
                    fXML.WriteStartAttribute("changed")
                    fXML.WriteValue(Date.Now.ToString("yyyy-MM-dd hh:mm:ss"))
                    fXML.WriteStartAttribute("uid")
                    fXML.WriteValue("{" & Guid.NewGuid.ToString & "}")
                    fXML.WriteStartElement("Properties")
                    fXML.WriteStartAttribute("action")
                    fXML.WriteValue("U")
                    fXML.WriteStartAttribute("displayDecimal")
                    fXML.WriteValue("0")
                    fXML.WriteStartAttribute("default")
                    fXML.WriteValue("0")
                    fXML.WriteStartAttribute("hive")
                    fXML.WriteValue(Mid(key, 2, InStr(key, "\") - 2))
                    fXML.WriteStartAttribute("key")
                    fXML.WriteValue(Mid(key, InStr(key, "\") + 1, Len(key) - InStr(key, "\") - 1))
                    fXML.WriteStartAttribute("name")
                    fXML.WriteValue(element.name)
                    fXML.WriteStartAttribute("type")
                    fXML.WriteValue(element.type)
                    fXML.WriteStartAttribute("value")
                    fXML.WriteValue(element.value)
                    fXML.WriteEndElement()
                    fXML.WriteEndElement()

                Next

            End If

        Next

        ' Finalización y cierre del fichero xml
        fXML.WriteEndElement()
        fXML.WriteEndDocument()
        fXML.Flush()
        fXML.Close()

    End Sub

End Module
