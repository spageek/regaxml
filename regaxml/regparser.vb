﻿' Pequeña librería de funciones para tratar los ficheros con formato reg.

Imports System.IO
Imports System.Text.RegularExpressions

Module regparser

    ' Definimos una estructura para las keys, por defecto todas las keys tienen un valor
    ' clsid fijo de 9CD4B2F4-923D-47f5-A062-E897DD1DAD50.
    Structure item
        Const clsid As String = "{9CD4B2F4-923D-47f5-A062-E897DD1DAD50}"
        Dim name As String
        Dim type As String
        Dim value As String
        Dim image As Integer

    End Structure
    ' Función que cuenta las entradas clave según las lineas que empiecen por "[HKEY_" y 
    ' finalicen por "]"
    Public Function countKeys(ByVal file As String) As Integer

        Dim fInput As New StreamReader(file)
        Dim line As String = fInput.ReadLine
        Dim counter As Integer = 0

        While Not fInput.EndOfStream
            If Regex.IsMatch(line, "^\[HKEY_.*\]$") Then counter += 1
            line = fInput.ReadLine
        End While

        fInput.Close()

        Return counter

    End Function
    ' Función para recoger una entrada del registro por posición numérica
    Public Function getKey(ByVal file As String, ByVal numKey As Integer) As String

        Dim fInput As New StreamReader(file)
        Dim line As String = fInput.ReadLine
        Dim counter As Integer = 0

        While Not fInput.EndOfStream
            If Regex.IsMatch(line, "^\[HKEY_.*\]$") Then counter += 1
            If counter = numKey Then Return line
            line = fInput.ReadLine
        End While

        fInput.Close()

        Return "Key number doesn't exist."

    End Function
    ' Función para recoger todas las entradas clave del fichero en un array
    Public Function getKeys(ByVal file As String) As String()

        Dim fInput As New StreamReader(file)
        Dim line As String = Trim(fInput.ReadLine)
        Dim counter As Integer = 0
        Dim keys() As String = {}

        While Not fInput.EndOfStream
            If Regex.IsMatch(line, "^\[HKEY_.*\]$") Then
                ReDim Preserve keys(counter)
                keys(counter) = line
                counter += 1
            End If
            line = fInput.ReadLine
        End While

        fInput.Close()

        Return keys

    End Function
    ' Devuelve el contenido de una clave de registro existente en el fichero
    Public Function getKeyContent(ByVal file As String, ByVal Key As String) As item()

        Dim fInput As New StreamReader(file)
        Dim line As String = fInput.ReadLine
        Dim items() As item = {}
        Dim counter As Integer = 0
        Dim i As Integer = 0

        While Not fInput.EndOfStream
            If line = Key Then
                line = fInput.ReadLine

                While Regex.IsMatch(line, """.*""=")

                    If Regex.IsMatch(line, """.*""=""") Then
                        ReDim Preserve items(i)
                        items(i).type = "REG_SZ"
                        items(i).name = Split(line, "=", 2)(0)
                        items(i).value = Split(line, "=", 2)(1)
                        While Mid(line, Len(line), 1) = "\"
                            line = Trim(fInput.ReadLine)
                            items(i).value = Mid(items(i).value, 1, Len(items(i).value) - 1) & Trim(line)
                        End While
                        items(i).image = 7
                    ElseIf Regex.IsMatch(line, """.*""=dword") Then
                        ReDim Preserve items(i)
                        items(i).type = "REG_DWORD"
                        items(i).name = Split(line, "=", 2)(0)
                        items(i).value = Split(line, "=", 2)(1)
                        While Mid(line, Len(line), 1) = "\"
                            line = Trim(fInput.ReadLine)
                            items(i).value = items(i).value = Mid(items(i).value, 1, Len(items(i).value) - 1) & Trim(line)
                        End While
                        items(i).value = Replace(items(i).value, "dword:", "")
                        items(i).image = 12
                    ElseIf Regex.IsMatch(line, """.*""=hex") Then
                        ReDim Preserve items(i)
                        items(i).type = "REG_BINARY"
                        items(i).name = Split(line, "=", 2)(0)
                        items(i).value = Split(line, "=", 2)(1)
                        While Mid(line, Len(line), 1) = "\"
                            line = Trim(fInput.ReadLine)
                            items(i).value = Mid(items(i).value, 1, Len(items(i).value) - 1) & Trim(line)
                        End While
                        items(i).value = Replace(Replace(items(i).value, ",", ""), "hex:", "")
                        items(i).image = 17
                    End If

                    If Regex.IsMatch(items(i).value, "^"".*""$") Then items(i).value = Mid(items(i).value, 2, Len(items(i).value) - 2)
                    If Regex.IsMatch(items(i).name, "^"".*""$") Then items(i).name = Mid(items(i).name, 2, Len(items(i).name) - 2)

                    i += 1
                    line = Trim(fInput.ReadLine)

                End While
                
            End If


            line = fInput.ReadLine

        End While

        fInput.Close()

        Return items

    End Function

End Module
